ARG BASE_IMAGE="debian:bullseye-slim"
FROM ${BASE_IMAGE} as builder

RUN set -ex; \
    export DEBIAN_FRONTEND="noninteractive"; \
    apt-get -yq update; \
    apt-get -yq full-upgrade; \
    apt-get install -yq --no-install-recommends build-essential git gnutls-dev libc6-dev libcanberra-dev \
    libcanberra-gtk3-dev libgccjit-10-dev libgif-dev libgtk-3-dev libgtk2.0-dev \
    libjansson-dev libjpeg-dev libjpeg62-turbo libmagickwand-dev libncurses-dev \
    libncurses5-dev libpng-dev libtiff-dev libtiff5-dev libwebkit2gtk-4.0-dev \
    libx11-dev libxpm-dev texinfo xaw3dg-dev zlib1g-dev; \
    apt-get -yq clean; \
    apt-get -yq autoclean; \
    apt-get -yq autoremove; \
    rm -rf /var/lib/apt/lists/*; \
    # Clone, compile and install emacs, then remove sources files
    mkdir -p /opt/sources; \
    cd /opt/sources; \
    git clone git://git.savannah.gnu.org/emacs.git; \
    cd /opt/sources/emacs; \
    ./autogen.sh; \
    ./configure --with-modules --with-mailutils --with-cairo --with-xwidgets --with-x-toolkit=gtk3 --with-native-compilation --prefix=/opt/emacs; \
    make NATIVE_FULL_AOT=1 -j$(nproc); \
    make install-strip; \
    rm -rf /opt/sources;

FROM ${BASE_IMAGE} as runtime

# Default user and group identifiers
ARG USER_UID="1000"
ARG USER_GID="1000"

WORKDIR /opt/emacs

COPY --from=builder /opt/emacs .

RUN set -ex; \
    export DEBIAN_FRONTEND="noninteractive"; \
    apt-get -yq update; \
    apt-get -yq full-upgrade; \
    apt-get install -yq --no-install-recommends binutils coreutils git \
    libc6-dev libcanberra-gtk3-0 libcanberra-gtk3-module libgccjit0 libgif7 \
    libgmp10 libgnutlsxx28 libgtk-3-0 libgtk-3-bin libjansson4 \
    libjpeg62-turbo libmpc3 libmpfr6 libncurses5 libpng16-16 libsm6 \
    libtiff5 libwebkit2gtk-4.0-37 libx11-xcb1 libxml2 libxpm4 libxt6 librsvg2-bin; \
    apt-get -yq clean; \
    apt-get -yq autoclean; \
    apt-get -yq autoremove; \
    rm -rf /var/lib/apt/lists/*;

# Create group and user
RUN groupadd --gid "${USER_GID}" "group";
RUN useradd -m user --uid "${USER_UID}" --gid "${USER_GID}"

# Set current user to user:group
USER user:group

WORKDIR /home/user

ENTRYPOINT ["/opt/emacs/bin/emacs"]
