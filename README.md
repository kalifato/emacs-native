# README

POC to create a native emacs docker image.

# Build docker image
```
docker build -t kalifato/emacs-native .
```

# Usage
```
docker run --rm  -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY kalifato/emacs-native
```
# efficency score
```
docker run --rm -it \
    -v /var/run/docker.sock:/var/run/docker.sock \
    wagoodman/dive:latest kalifato/emacs-native
```